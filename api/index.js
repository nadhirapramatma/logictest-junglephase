import config from 'dotenv'
import express from 'express';
import bodyParser from 'body-parser';
import passport from 'passport';
import session from 'express-session';

import swaggerJsDoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';

//import router
import roleRoutes from './server/routes/RoleRoutes';
import userRoutes from './server/routes/UserRoutes';
import userdetailRoutes from './server/routes/userdetailRoutes';  
import questionRoutes from './server/routes/QuestionRoute';
import formAnswerRoutes from './server/routes/FormAnswerRoute';
import userExamRoutes from './server/routes/UserExamRoute';
import testTypeRouter from './server/routes/testTypeRouter';
import questionCategoryRouter from './server/routes/questionCategoryRouter'
import questionTypeRouter from './server/routes/questionTypeRoute'


config.config();

require('./server/src/config/passport')(passport);

// import userRoutes from './server/routes/userRoutes';
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({ 
   secret: 'nodeauthsecret',
   resave: true,
   saveUninitialized: true})
);
app.use(passport.initialize());
app.use(passport.session());
const port = process.env.PORT || 8080;

// swagger configuration
const swaggerOptions = {
   swaggerDefinition: {
      info: {
         title: "Binar Academy Dashboard API",
         description: "Provides endpoint that related to academic activities",
         contact: {
            name: "Binar x Kominfo Batch 2",
         },
         servers: ["http://localhost:8080"]
      }
   },
   basePath: '/api/v1',
   apis:["./docs/**/*.yaml"]
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);


// api
app.use('/api/v1/role', roleRoutes);
app.use('/api/v1/user', userRoutes);
app.use('/api/v1/question', questionRoutes);
app.use('/api/v1/answer', formAnswerRoutes);
app.use('/api/v1/user-exam', userExamRoutes);
app.use('/api/v1/test-type', testTypeRouter);
app.use('/api/v1/question-category', questionCategoryRouter);
app.use('/api/v1/question-type', questionTypeRouter);
app.use('/api/v1/user-detail', userdetailRoutes);
app.use('/api/v1/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));
// app.use('/api/user', userRoutes);

// when a random route is inputed
app.get('*', (req, res, next) => res.status(200).send({
   message: 'Welcome to this API.'
}));

app.listen(port, () => {
   console.log(`Server is running on PORT ${port}`);
});
export default app;
