import express from 'express';
import jwt from 'jsonwebtoken';
import User from '../src/models/user';

async function isAuthenticated(req, res, next) {
    var token = req.body.token || req.query.token || req.headers.authorization;
    if (token) {
        jwt.verify(token, process.env.JWT_KEY, function (err, decoded) {
            if (error) {
                res.json({message: 'failed to authenticated token'});
            } else {
                req.decoded = decoded;
                next()
            }
        });
    } else {
        return res.status(403).send({message: 'No token provided'})
    }
}

module.exports = isAuthenticated;