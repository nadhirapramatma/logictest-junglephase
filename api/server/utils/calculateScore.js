const UserExam = require('../src/models').UserExam
const Answer = require('../src/models').FormAnswer
const Question = require('../src/models').Question

  exports.getScore = async function (req, res, next){
    try {
      
      const userExam= await UserExam.findByPk(req.params.id)
      console.log(userExam.dataValues, 'this is id from helper') 

      let scoreUser = userExam.score;
      console.log(scoreUser, 'scoreUser')

      const formAnswers = await Answer.findAll({where: {userExamId:req.params.id}}, {
        include: [
          {
            model: Question,
            as: 'question'
          }
        ]
      })
      console.log(formAnswers, 'this is all answers')
      
      // let scorePoin = formAnswers.answers.question.scoreQuestion
      // console.log(scorePoin, 'this is score point')

      formAnswers.forEach((formanswer) => {
        if (formanswer.isCorrect === true) {
          scoreUser += 10
        } else {
          scoreUser;
        }
      });

      let score = scoreUser;
      console.log(score, 'this is score')

      req.score = score;
      next()
    } catch (err){
        return res.status(505).json({message: err.message})
    }
    
}