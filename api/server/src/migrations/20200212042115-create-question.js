'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Questions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      questionCategoryId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'QuestionCategories', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      questionTypeId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'QuestionTypes', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      question: {
        type: Sequelize.STRING,
        allowNull: false 
      },
      options: {
        type: Sequelize.ARRAY(Sequelize.STRING)
      },
      answer: {
        type: Sequelize.STRING,
        allowNull: false
      },
      difficulty: {        
        allowNull: false, 
        type: Sequelize.ENUM({
          values:['easy', 'medium', 'advance']
        })
      },
      scoreQuestion: {        
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Questions');
  }
};