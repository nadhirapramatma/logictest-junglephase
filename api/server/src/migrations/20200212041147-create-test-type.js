'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('TestTypes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      }, 
      description: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      instruction: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      statusPublish: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      timer: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      correctPoint: {
        type: Sequelize.INTEGER,
      },
      falsePoint: {
        type: Sequelize.INTEGER,
      },
      emptyPoint: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('TestTypes');
  }
};