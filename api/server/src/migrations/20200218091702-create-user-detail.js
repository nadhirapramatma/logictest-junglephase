'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('UserDetails', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Users', 
          key: 'id', 
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
      fullname: {
        allowNull: false,
        type: Sequelize.STRING
      },
      nickname: {
        type: Sequelize.STRING
      },
      dob: {
        allowNull: false,
        type: Sequelize.STRING
      },
      last_education: {
        allowNull: false,
        type: Sequelize.ENUM({
          values:['SMP', 'SMA', 'Diploma', 'S1','S2', 'S3']
        }) 
      },
      majors: {
        type: Sequelize.STRING
      },
      school_name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      city_id: {
        type: Sequelize.STRING
      },
      province_id: {
        type: Sequelize.STRING
      },
      full_address_id: {
        type: Sequelize.STRING
      },
      domicile_city: {
        type: Sequelize.STRING
      },
      domicile_province: {
        type: Sequelize.STRING
      },
      domicile_full_address: {
        type: Sequelize.STRING
      },
      join_motivation: {
        type: Sequelize.STRING
      },
      join_motivation_desc: {
        type: Sequelize.STRING
      },
      data_usage_agreement: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('UserDetails');
  }
};
