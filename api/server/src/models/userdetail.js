'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserDetail = sequelize.define('UserDetail', {
    userId: {
      type: DataTypes.INTEGER,
    },
    fullname: {
      type: DataTypes.STRING,
    },
    nickname: {
      type: DataTypes.STRING,
    },
    dob: {
      type: DataTypes.STRING,
    },
    last_education: {
      type: DataTypes.STRING,
    },
    majors: {
      type: DataTypes.STRING,
    },
    school_name: {
      type: DataTypes.STRING,
    },
    city_id: {
      type: DataTypes.STRING,
    },
    province_id: {
      type: DataTypes.STRING,
    },
    full_address_id: {
      type: DataTypes.STRING,
    },
    domicile_city: {
      type: DataTypes.STRING,
    },
    domicile_province: {
      type: DataTypes.STRING,
    },
    domicile_full_address: {
      type: DataTypes.STRING,
    },
    linkedin_url: {
      type: DataTypes.STRING,
    },
    join_motivation: {
      type: DataTypes.STRING,
    },
    join_motivation_desc: {
      type: DataTypes.STRING,
    },
    data_usage_agreement: {
      type: DataTypes.BOOLEAN
    }
  }, {});

  UserDetail.associate = function(models) {
    // associations can be defined here
    UserDetail.belongsTo(models.User, {
      foreignKey: 'userId',
      sourceKey: 'userId',
      as: 'user'
    });
    UserDetail.hasMany(models.UserExam, {
      foreignKey:'userDetailId',
      as:'UserExam'
    })
  };
  return UserDetail;
};