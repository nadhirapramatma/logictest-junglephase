'use strict';
import bcrypt from 'bcryptjs';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    roleId: DataTypes.INTEGER,
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: {
        args: true,
        msg: 'Your email account already used'
      },
      validate: {
        isEmail: {
          args: true,
          msg: 'please input a valid email account'
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: {
          args: 7,
          msg: 'Password must be more than 7 characters'
        }
      }
    },
    phone_number: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        unique: function(value, next) {
         User.findOne({
           where: {
             phone_number: value
           }
         }).then(function(result) {
           if(result === null) {
             return next()
           } else {
             return next('phone number already exist')
           }
         }).catch(error => {
           return next()
         });
        },
        isNumeric: {
          args: true,
          msg: 'please input a valid phone number'
        }
      }
      
    },
    service_agreement: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    data_usage_agreement: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {});

  User.beforeSave((user, options) => {
    if (user.changed('password')) {
      user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(10), null);
    }
  });

  User.prototype.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.password, function(err, isMatch) {
      if (err) {
        return cb(err);
      }
      cb(null, isMatch);
    });
  };

  // User.beforeCreate(function(user, password) {
  //   if (user.password.length < 8) {
    //   }
  //     throw new Error("Password character has to be 8 or more")
  // });

  User.associate = function(models){
    // associations can be defined here
    User.belongsTo(models.Role,{
      foreignKey: 'roleId',
      sourceKey: 'roleId',
      as: 'role'});
    User.hasOne(models.UserDetail, {
      foreignKey: 'userDetailId',
      as: 'userdetail'
    })
  };
  return User;
};