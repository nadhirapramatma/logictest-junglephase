'use strict';
module.exports = (sequelize, DataTypes) => {
  const QuestionType = sequelize.define('QuestionType', {
    name:  {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {});
  QuestionType.associate = function(models) {
    // associations can be defined here
    QuestionType.hasMany(models.Question,{
      foreignKey:'questionTypeId'
    });
  };
  return QuestionType;
};