module.exports = (sequelize, DataTypes) => {
  const TestType = sequelize.define('TestType', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        notNull: {
          msg: 'Please input test type name'
        }
      },
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
    }, 
    instruction: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    statusPublish: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    timer:{
      type: DataTypes.STRING,
      allowNull: false,
    },
    correctPoint:{
      type: DataTypes.INTEGER,
    },
    falsePoint: {
      type: DataTypes.INTEGER,
    },
    emptyPoint: {
      type: DataTypes.INTEGER,
    },
  }, {});
  TestType.associate = function(models) {
    // associations can be defined here
    TestType.hasMany(models.QuestionCategory, {
      foreignKey: 'testTypeId',
    });

    TestType.hasMany(models.UserExam, {
      foreignKey:'testId',
      as:'test'
    })
  };
  return TestType;
};