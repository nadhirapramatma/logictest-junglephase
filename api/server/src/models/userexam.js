'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserExam = sequelize.define('UserExam', {
    userDetailId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    testId:{
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    score: DataTypes.INTEGER
    // dateStart :DataTypes.DATE,  
    // dateFinish:DataTypes.DATE,
    
  }, {});
  UserExam.associate = function(models) {
    // associations can be defined here
    UserExam.belongsTo(models.UserDetail,{
      foreignKey:'userDetailId',
      as:'userdetail'
    });

    UserExam.belongsTo(models.TestType,{
        foreignKey:'testId',
        as:'testtype'
      });

    UserExam.hasMany(models.FormAnswer,{
      foreignKey:'userExamId',
      as:'formanswer'
    });
  };
  return UserExam;
};
