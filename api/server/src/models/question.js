'use strict';
module.exports = (sequelize, DataTypes) => {
  const Question = sequelize.define('Question', {
    questionCategoryId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    questionTypeId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    question:{
      type: DataTypes.STRING,
      allowNull: false,
    },
    options: DataTypes.ARRAY(DataTypes.STRING),
    answer: DataTypes.STRING,
    difficulty: DataTypes.ENUM({
      values:['easy', 'medium', 'advance']
    }),
    scoreQuestion:DataTypes.INTEGER
  }, {});
  Question.associate = function(models) {
    // associations can be defined here
    Question.belongsTo(models.QuestionType,{
      foreignKey:'questionTypeId',
      as:'questionType'
    });

    Question.belongsTo(models.QuestionCategory,{
      foreignKey:'questionCategoryId',
      as:'questionCategory'
    });

    Question.hasMany(models.FormAnswer,{
      foreignKey:'questionId',
      as:'formanswers'
    });
  };
  return Question;
};
