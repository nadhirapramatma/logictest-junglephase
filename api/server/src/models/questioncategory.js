'use strict';
module.exports = (sequelize, DataTypes) => {
  const QuestionCategory = sequelize.define('QuestionCategory', {
    name:  {
      type: DataTypes.STRING,
      allowNull: false,
    },
    testTypeId:{
      type: DataTypes.INTEGER,
    },
  }, {});
  QuestionCategory.associate = function(models) {
    // associations can be defined here
    QuestionCategory.belongsTo(models.TestType, {
      foreignKey: 'testTypeId',
      as: 'TestType',
    });

    QuestionCategory.hasMany(models.Question,{
      foreignKey:'questionCategoryId'
    });
  };
  return QuestionCategory;
};
