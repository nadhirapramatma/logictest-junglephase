'use strict';

module.exports = (sequelize, DataTypes) => {
  const FormAnswer = sequelize.define('FormAnswer', {
    userExamId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    questionId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    answer: DataTypes.STRING,
    isCorrect: DataTypes.BOOLEAN
  }, {});



  //add beforecreate hooks to assign iscorrect field

  FormAnswer.beforeBulkCreate((formanswers, questionId) => {
		formanswers.forEach((formanswer) => {
      if (formanswer.answer === formanswer.questionId[questionId]) {
        formanswer.isCorrect = true
      } else {
        formanswer.isCorrect = false;
      }
    });
    }
  )

 
  FormAnswer.associate = function(models) {

    // associations can be defined here
    FormAnswer.belongsTo(models.Question,{
      foreignKey:'questionId',
      as:'question'
    });

    FormAnswer.belongsTo(models.UserExam,{
      foreignKey:'userExamId',
      as:'userexam'
    });
  };
  return FormAnswer;
};
