const LocalStrategy = require('passport-local').Strategy;
var JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;

    //import user model
const User = require('../models').User;

module.exports = function(passport) {
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(user, done) {
        done(null, user);
        // models.User.findOne({
        //     where: {id: (id)}
        // })
    });

    //jwt
    const opts = {
        jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('JWT'),
        secretOrKey: 'nodeauthsecret',
    };
    passport.use('jwt', new JwtStrategy(opts, function(jwt_payload, done) {
        console.log(jwt_payload)
        User
            .findByPk({id: jwt_payload.id})
            .then((user) => { return done(null, user); })
            .catch((error) => { return done(error, false); });
    }
    ));
    
};