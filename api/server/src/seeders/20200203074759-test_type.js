module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert(
      "TestTypes",
      [
        {
          name: 'logic5',
          description: 'xxxxxxxxxxxx1',
          instruction: 'blablablabla',
          statusPublish: true,
          timer: '120 minutes',
          correctPoint: 4,
          falsePoint: 1,
          emptyPoint: 0,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'logic6',
          description: 'xxxxxxxxxxxx2',
          instruction: 'blablablabla',
          statusPublish: false,
          timer: '90 minutes',
          correctPoint: 4,
          falsePoint: 1,
          emptyPoint: 0,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    ),

  down: (queryInterface, Sequelize) =>
    queryInterface.bulkDelete("TestTypes", null, {})
};
