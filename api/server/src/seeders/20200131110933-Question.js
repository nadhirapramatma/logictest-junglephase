'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert(
    'Questions',
    [
      {
        questioncategoryId: '1',
        question: 'jika x + 7 = 8, tentukan nilai dari x ?',
        options: ['3', '4', '18'],
        answer: '1',
        difficulty: 'easy',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        questioncategoryId: '2',
        question: 'berapa nilai dari 2 x 4 ?',
        options: ['12', '9', '20'],
        answer: '8',
        difficulty: 'easy',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        questioncategoryId: '1',
        question: '4 / y = 2, tentukan nilai dari y ?',
        options: ['3', '1', '4'],
        answer: '2',
        difficulty: 'easy',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        questioncategoryId: '2',
        question: 'berapa nilai dari 7 x 4 ?',
        options: ['32', '21', '20'],
        answer: '28',
        difficulty: 'easy',
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    ],
    {},
  ),

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('Questions', null, {}),
};