module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert(
      "QuestionTypes",
      [
        {
          name: 'pilihan',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'essay',
          createdAt: new Date(),
          updatedAt: new Date(),
        }
      ],
      {}
    ),

  down: (queryInterface, Sequelize) =>
    queryInterface.bulkDelete("QuestionTypes", null, {})
};