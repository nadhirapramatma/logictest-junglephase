'use strict';

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert(
      "Questions",
      [
        {
          questionCategoryId: 1,
          questionTypeId:1,
          question:'test pertanyaan 1',
          options:['a', 'b', 'c'],
          answer:'d',
          difficulty:'easy',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          questionCategoryId: 2,
          questionTypeId:1,
          question:'test pertanyaan 2',
          options:['a', 'b', 'd'],
          answer:'c',
          difficulty:'medium',
          createdAt: new Date(),
          updatedAt: new Date(),
        }
      ],
      {}
    ),

  down: (queryInterface, Sequelize) =>
    queryInterface.bulkDelete("Questions", null, {})
};