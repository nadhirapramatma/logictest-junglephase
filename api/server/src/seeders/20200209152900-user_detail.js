'use strict';

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert(
      "UserDetails",
      [
        {
          fullname: 'kurnia',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          fullname: 'diah',
          createdAt: new Date(),
          updatedAt: new Date(),
        }
      ],
      {}
    ),

  down: (queryInterface, Sequelize) =>
    queryInterface.bulkDelete("UserDetails", null, {})
};