module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert(
      "QuestionCategories",
      [
        {
          name: 'perulangan',
          testTypeId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'aritmatika',
          testTypeId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        }
      ],
      {}
    ),

  down: (queryInterface, Sequelize) =>
    queryInterface.bulkDelete("QuestionCategories", null, {})
};
