'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert(
    'Questioncategories',
    [
      {
        name: 'aljabar',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'aritmatika',
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    ],
    {},
  ),

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('Questioncategories', null, {}),
};