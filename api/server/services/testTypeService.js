
import models from '../src/models';

class testTypeService {
  static async getAllTestType(){
    try{
      return models.TestType.findAll()
    }catch(error){
      throw error
    }
  }
  
  static async addTestType(newTestType){
    try{
      return await models.TestType.create(newTestType);
    }catch(error){
      throw error;
    }
  }

  static async updateBook(id, updateBook) {
    try {
      const bookToUpdate = await database.Book.findOne({
        where: { id: Number(id) }
      });

      if (bookToUpdate) {
        await database.Book.update(updateBook, { where: { id: Number(id) } });

        return updateBook;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async updateTestType(id, updateTestType) {
    try{
      const testTypeToUpdate = await models.TestType.findOne({
        where: {id: Number(id)}
      }) 

      if(testTypeToUpdate){
        await models.TestType.update(updateTestType, {where: {id: Number(id)}})
        return updateTestType;
      }
      return null
    }catch(error){
      throw error
    }
  }

  static async getOneTestType(id){
    try{
      const theTestType = await models.TestType.findOne({
        where: { id: Number(id)}
      })
      return theTestType;
    }catch(error){
      throw(error)
    }
  }

  static async deleteTestType(id){
    try{
      const testTypeToDelete = await models.TestType.findOne({where: {id: Number(id)}})
      
      if(testTypeToDelete){
        const deletedTestType = await  models.TestType.destroy({
          where: {id: Number(id)}
        });
        return deletedTestType;
      }
      return null;
    }catch(error){
      throw error
    }
  }
}

export default testTypeService;