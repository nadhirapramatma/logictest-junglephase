import database from '../src/models';

class UserExamService {

  //get all userexam
  static async getAllUserexams() {
    try {
      return await database.UserExam.findAll({
        include: [
            {
              model: database.UserDetail,
              as: 'examine'
            },
            {
              model: database.TestType,
              as: 'test'
            }
          ]
      });
    } catch (error) {
      throw error;
    }
  }

  //create new userexam
  static async createUserexam(newUserexam) {
    try {
      return await database.UserExam.create(newUserexam);
    } catch (error) {
      throw error;
    }
  }

  //update user exam
  static async updateUserexam(id, updateUserExam) {
    try {
      const userExamToUpdate = await database.UserExam.findOne({
        where: { id: Number(id) }
      });

      if (userExamToUpdate) {
        await database.UserExam.update(updateUserExam, { where: { id: Number(id) } });

        return updateUserExam;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

}

export default UserExamService;