import database from '../src/models';
import express from 'express';

class userService {
    static async addUser(req, res) {
      try {
        const newuser = await database.User.create(req.body);
        return res.status(201).json({
          newuser,
        });
      } catch (error) {
        return res.status(500).json({error: error.message})
      }
    }
  
    static async getAllUsers(req, res) {
      try {
        const users = await database.User.findAll({
          include: [
            {
              model: models.userdetail,
              as: 'User'
            }
          ]
        });
        return res.status(200).json({users});
      } catch (error) {
        return res.status(500).send(error.message);
      }
    }
  
    static async getAUser(req, res) {
      try {
        // const {userId} = req.params;
        const user = await models.User.findOne({
          where: { id: Id },
          include: [
            {
              model: models.userDetail,
              as: 'user'
            }
          ]
        });
        if (user) {
          return res.status(200).json({user});
        }
        return res.status(404).send('User with the specified id doesnt exist');
      } catch (error) {
        return res.status(500).send(error.message);
      }
    }
  
    static async updateUser(req, res) {
      try {
        // const {userId} = req.params;
        const {updated} = await database.User.update(req.body, {
          where: {id: Id}
        });
        if (updated) {
          const updateduser = await database.User.findOne({
            where: {id: Id}
          });
          return res.status(200).json({user:updateduser});
        }
        throw new Error('User not found');
      } catch (error) {
        return res.status(500).send(error.message);
      }
    };
  
    static async deleteUser(req, res) {
      try {
        // const {userId} = req.params;
        const deleted = await database.User.destroy({
          where: {id: Id}
        });
        if (deleted) {
          return res.status(204).send('User deleted');
        }
        throw new Error('user not found');
      } catch (error) {
        return res.status(500).send(error.message);
      }
    };
  }
  
  export default userService;
  
