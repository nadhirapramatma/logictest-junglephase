import database from '../src/models';
// import express from 'express';

class userdetailService {
    static async getAllUsersDetail() {
      try {
        return await database.userDetail.findAll({
        //   include: [
        //   {
        //     model: models.Role,
        //     as: 'Role'
        //   }
        // ]
        });
      } catch (error) {
        throw error;
      }
    }
  
    static async addUserDetail(newUser) {
      try {
        return await database.userDetail.create(newUser);
      } catch (error) {
        throw error;
      }
    }
  
    static async updateUserDetail(id, updateUserDetail) {
      try {
        const updateUserDetail = await database.userDetail.findOne({
          where: { id: Number(id) }
        });
  
        if (updateUserDetail) {
          await database.userDetail.update(updateUserDetail, { where: { id: Number(id) } });  
          console.log(updateUserDetail);
          console.log(id)
          return updateUserDetail;
        }
        
        return null;
      } catch (error) {
        throw error;
      }
    }
  
    static async getAUserDetail(id) {
      try {
        const user = await database.userDetail.findOne({
          where: { id: Number(id) },
        //   include: [
        //     {
        //     model: models.Role,
        //     as: 'Role'
        //   }
        // ]
        });
  
        return user;
      } catch (error) {
        throw error;
      }
    }
  
    static async deleteUserDetail(id) {
      try {
        const deleted = await database.userDetail.findOne({ where: { id: Number(id) } });
  
        if (deleted) {
          const deleteUserDetail = await database.userDetail.destroy({
            where: { id: Number(id) }
          });
          return deleteUserDetail;
        }
        return null;
      } catch (error) {
        throw error;
      }
    }

  }
  
  export default userdetailService;
