import database from '../src/models';

class RoleService {
    static async getAllRoles() {
      try {
        return await database.Role.findAll();
      } catch (error) {
        throw error;
      }
    }
  
    static async addRole(newRole) {
      try {
        return await database.Role.create(newRole);
      } catch (error) {
        throw error;
      }
    }
  
    static async updateRole(id, updateRole) {
      try {
        const roleToUpdate = await database.Role.findOne({
          where: { id: Number(id) }
        });
  
        if (roleToUpdate) {
          await database.Role.update(updateRole, { where: { id: Number(id) } });
          console.log(id)
          console.log(updateRole)
          return updateRole;
        }
        return null;
      } catch (error) {
        throw error;
      }
    }
  
    static async getARole(id) {
      try {
        const theRole = await database.Role.findOne({
          where: { id: Number(id) }
        });
  
        return theRole;
      } catch (error) {
        throw error;
      }
    }
  
    static async deleteRole(id) {
      try {
        const roleToDelete = await database.Role.findOne({ where: { id: Number(id) } });
  
        if (roleToDelete) {
          const deletedRole = await database.Role.destroy({
            where: { id: Number(id) }
          });
          return deletedRole;
        }
        return null;
      } catch (error) {
        throw error;
      }
    }
  }
  
  export default RoleService;
  