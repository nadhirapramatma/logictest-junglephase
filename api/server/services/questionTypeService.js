import models from '../src/models'

class questionTypeService{
    static async addQuestionType(newQuestionType){
        try{
            return await models.QuestionType.create(newQuestionType);
        }catch(error){
            throw (error);
        }
    }

    static async getOneQuestionType(id){
        try{
            const theQuestionType =  await models.QuestionType.findOne({
                where: {id: Number(id)}
            })
            return theQuestionType;
        }catch(error){
            throw (error)
        }
    }

    static async getAllQuestionTypes(){
        try{
            return models.QuestionType.findAll()
        }catch(error){
            throw (error)
        }
    }

    static async updateQuestionType(id, updateQuestionType) {
        try{
          const questionTypeToUpdate = await models.QuestionType.findOne({
            where: {id: Number(id)}
          }) 
    
          if(questionTypeToUpdate){
            await models.QuestionType.update(updateQuestionType, {where: {id: Number(id)}})
            return updateQuestionType;
          }
          return null
        }catch(error){
          throw error
        }
      }

    static async deleteOneQuestionType(id){
        try{
            const questionTypeToDelete = await models.QuestionType.findOne({where: {id: Number(id)}})
            
            if(questionTypeToDelete){
              const deletedQuestionType = await  models.QuestionType.destroy({
                where: {id: Number(id)}
              });
              return deletedQuestionType;
            }
            return null;
          }catch(error){
            throw error
          }
    }
}

export default questionTypeService;