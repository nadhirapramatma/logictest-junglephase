import database from '../src/models';

class questionService {

  //get all question
  static async getAllQuestions() {
    try {
      return await database.Question.findAll({
        include: [
          {
            model: models.question_type,
            as: 'questionType'
          },
          {
            model: models.question_category,
            as: 'questionCategory'
          }
        ]
      });
    } catch (error) {
      throw error;
    }
  }

  //create new question
  static async createQuestion(newQuestion) {
    try {
      return await database.Question.create(newQuestion);
    } catch (error) {
      throw error;
    }
  }

  //update question 
  static async updateQuestion(id, updateQuestion) {
    try {
      const questionToUpdate = await database.Question.findOne({
        where: { id: Number(id) }
      });

      if (questionToUpdate) {
        await database.Question.update(updateQuestion, { where: { id: Number(id) } });

        return updateQuestion;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

}

export default questionService;