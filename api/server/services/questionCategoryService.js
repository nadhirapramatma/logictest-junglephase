import models from '../src/models'

class questionCategoryService{
    static async addQuestionCategory(newQuestionCategory){
        try{
            return await models.QuestionCategory.create(newQuestionCategory);
        }catch(error){
            throw (error);
        }
    }

    static async getOneQuestionCategory(id){
        try{
            const theQuestionCategory = await models.QuestionCategory.findOne({
                where: { id: Number(id) },
                include:[{
                    model: TestType,
                    as: 'TestType'
                }]
                
            })
            return theQuestionCategory;
        }catch(error){
            throw (error)
        }
    }
}

export default questionCategoryService;