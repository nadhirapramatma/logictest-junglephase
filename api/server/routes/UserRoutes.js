import {Router} from 'express';
import UserController from '../controllers/UserController';
import passport from 'passport';
const myPassport = require('../src/config/passport')(passport);

const router = Router();

router.get('/', UserController.getAllUsers);
router.post('/', UserController.addUser, 
    passport.authenticate('jwt', {
        successRedirect: '/login',
        failureRedirect: '/',
        failureFlash: true,
    })
    );
// router.post()
router.post('/login', UserController.loginUser,
    passport.authenticate('jwt', {
        successRedirect: '/',
        failureRedirect: '/login' 
    }));
router.get('/current', UserController.currentUser)
router.put('/:id', UserController.updateUser);
router.get('/:id', UserController.getAUser);
router.delete('/:id', UserController.deleteUser);

export default router;
