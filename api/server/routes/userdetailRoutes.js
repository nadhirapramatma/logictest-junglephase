import {Router} from 'express';
import userdetailController from '../controllers/userdetailController';
import bodyParser from 'body-parser';


const router = Router();
router.use(bodyParser.json());

router.get('/', userdetailController.getAllUsersDetail);
router.post('/', userdetailController.addUserDetail);
router.put('/:id', userdetailController.updateUserDetail);
router.get('/:id', userdetailController.getAUserDetail);
router.delete('/:id', userdetailController.deleteUserDetail);

export default router;
