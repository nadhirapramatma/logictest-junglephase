import { Router } from 'express';
import UserexamController from '../controllers/UserexamController';
import helper from '../utils/calculateScore'

const router = Router();

router.get('/', UserexamController.getAllUserexams);
router.post('/', UserexamController.createUserexam);

router.get('/:id', UserexamController.getUserexamById);
router.put('/:id', UserexamController.updateUserExam);

router.put('/score/:id', helper.getScore , UserexamController.updateUserExamScore);

router.delete('/:id', UserexamController.deleteUserexam);

export default router;