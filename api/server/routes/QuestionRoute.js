import { Router } from 'express';
import QuestionController from '../controllers/QuestionController';

const router = Router();

router.get('/', QuestionController.getAllQuestions);
router.get('/:id', QuestionController.getQuestionById);

router.post('/', QuestionController.createQuestion);
router.delete('/:id', QuestionController.deleteQuestion);

router.put('/:id', QuestionController.updateQuestion);


export default router;