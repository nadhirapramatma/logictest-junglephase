import { Router} from 'express';
import questionCategoryController from '../controllers/questionCategoryController';

const router = Router();

router.get('/', (req,res) => res.send('halo, you are on question category table'));
router.post('/', questionCategoryController.addQuestionCategory);
router.get('/getAll', questionCategoryController.getAllQuestionCategories);
router.put('/:id ', questionCategoryController.updateQuestionCategoryById)
router.get('/:id', questionCategoryController.getOneQuestionCategory)
router.delete('/:id', questionCategoryController.deleteOneQuestionCategory)

export default router;