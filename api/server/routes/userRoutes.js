import {Router} from 'express';
import userController from '../controllers/userController';
import bodyParser from 'body-parser';

const router = Router();
router.use(bodyParser.json());

router.get('/', userController.getAllUsers);
router.post('/', userController.addUser);
router.put('/:id', userController.updateUser);
router.get('/:id', userController.getAUser);
router.delete('/:id', userController.deleteUser);

export default router;