import { Router} from 'express';
import testTypeController from '../controllers/testTypeController';

const router = Router();

router.get('/', (req,res) => res.send('halo, you are on test type table'));
router.post('/', testTypeController.addTestType);
router.get('/getAll', testTypeController.getAllTestTypes);
router.put('/:id', testTypeController.updateTestType)
router.get('/:id', testTypeController.getOneTestType)
router.delete('/:id', testTypeController.deleteOneTestType)
export default router;