import { Router} from 'express';
import questionTypeController from '../controllers/questionTypeController';

const router = Router();

router.get('/', (req,res) => res.send('halo, quetion type table'));
router.post('/', questionTypeController.addQuestionType);
router.get('/getAll', questionTypeController.getAllQuestionTypes);
router.put('/:id', questionTypeController.updateQuestionTypeById)
router.get('/:id', questionTypeController.getOneQuestionType)
router.delete('/:id', questionTypeController.deleteOneQuestionType)

export default router;