import { Router } from 'express';
//import { getStatusAnswer } from '../utils/statusanswer';
import FormanswerController from '../controllers/FormanswerController.js';

const router = Router();

router.get('/', FormanswerController.getAllAnswer);
router.get('/:id', FormanswerController.getAnswerById);

router.post('/one',  FormanswerController.createOneAnswer);
router.post('/all',  FormanswerController.createAllAnswersofOneUser);

router.delete('/:id', FormanswerController.deleteAnswer);
router.put('/:id', FormanswerController.updateAnswer);

export default router;