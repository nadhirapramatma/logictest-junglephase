import models from '../src/models';
import Util from '../utils/Utils';
import questionCategoryService from '../services/questionCategoryService';

const util = new Util();

class questionCategoryController{
  static async addQuestionCategory(req,res){
    if (!req.body.name || !req.body.testTypeId ) {
      util.setError(400, 'Please provide complete details');
      return util.send(res);
    }
    const newQuestionCategory = req.body
    try{
      const createdQuestionCategory = await questionCategoryService.addQuestionCategory(newQuestionCategory);
      util.setSuccess(201, 'Question Category success to added!', createdQuestionCategory);
      return util.send(res);
    }catch(error){
      util.setError(400, error.message);
      return util.send(res);
    }
  }

  static async getOneQuestionCategory(req,res){
    const {id} = req.params;
    if(!Number(id)){
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }

    try{
      const theQuestionCategory = await questionCategoryService.getOneQuestionCategory(id);

      if(!theQuestionCategory){
        util.setError(404, `Cannot find question category with the id ${id}`);
      }else{
        util.setSuccess(200, 'Found Question Type', theQuestionCategory);
      }
      return util.send(res);
    }catch(error){
      util.setError(400, error);
      return util.send(res);
    }
  }

  //controller update still didn't work
  static async updateQuestionCategoryById(req,res) {
    try{
      // const editquestionCategory = req.body;
      const { questionCategoryId } = req.params;
      if (!Number(questionCategoryId)) {
          util.setError(400, 'Please input a valid numeric value');
          return util.send(res);
        }
      const [updated] = await models.QuestionCategory.upsert({ name : req.body.name, where: {id:questionCategoryId}
      });

      if (updated) {
          const updatedquestionCategory = await models.QuestionCategory.findOne({where: {id:questionCategoryId}})
          return res.status(200).json({questionCategory : updatedquestionCategory})
      }
      throw new Error('quetion type not found');

    }catch(error){
        return res.status(500).send(error.message)
    }
  }

  // static async getOneQuestionCategory(req, res){
  //   const {id} = req.params;
  //   if (!Number(id)) {
  //       util.setError(400, 'Please input a valid numeric value');
  //       return util.send(res);
  //   }
  //   try{
  //       const onequestionCategory = await models.QuestionCategory.findOne(
  //         { where: {id: id}, 
  //           include: [
  //           {
  //             model: models.TestType,
  //             as: 'TestType'
  //           }]
  //         }
  //       )
  //       return res.status(200).json({ onequestionCategory });
  //   }catch(error){
  //       util.setError(404, error);
  //       return util.send(res);
  //   }
  // }

  static async getAllQuestionCategories  (req, res) {
    try {
      const questionType = await models.QuestionCategory.findAll({
        // include: [
        //   {
        //     model: models.TestType,
        //     as: 'TestType'
        //   }
        // ]
      });
      return res.status(200).json({ questionType });
    } catch (error) {
      return res.status(500).send(error.message);
    }
  }
    
  static async deleteOneQuestionCategory (req, res){
    try{
      const {questionTypeId} = req.params;
      const deleted = await models.QuestionCategory.destroy({where: {id: questionTypeId}})

      if(deleted){
          // const onequestionType = await models.QuestionType.findOne({ where: {id: id}})
          // return res.status(200).json({onequestionType})
          return res.status(204).send("one question type was deleted");
      } 
      throw new Error("question type not found");
    }catch(error){
      return res.status(500).send(error.message);
    }

    // controller deleted one row was success but didn't show success message
}
}

export default questionCategoryController;