import database from '../src/models';
import Role from '../src/models/role';
import Sequelize from 'sequelize';
import jwt from 'jsonwebtoken';
import passport from 'passport';
const myPassport = require('../src/config/passport')(passport);
const User = require('../src/models').User;
// require('../../config/passport')(passport);
// const util = new Util();

class UserController {

  static async addUser(req, res, next) {
    if(!req.body.name || !req.body.email || !req.body.password || !req.body.phone_number) {
       return res.status(400).send({message: 'Please complete the data'})
    }
    try {
        const newUser = await database.User.create(req.body);
        console.log(newUser)
        
        
       
       return res.status(201).send({message: 'Success', newUser})
      
    } catch (error) {
        return res.status(400).json({message: error.message})
    }
}
  

  static async getAllUsers(req, res) {
    try {
      // const Role = database.define('Roles', {type: Sequelize.INTEGER})
      const users = await database.User.findAll({
        
        include: [
          {
            model: database.Role,
            as: 'Role',
          }
        ]
      });
      return res.status(200).json({users});
    } catch (error) {
      return res.status(500).send({error: error.message});
    }
  }

  static async getAUser(req, res) {
    try {
      const { id } = req.params;
      const user = await database.User.findOne({
        where: { id: Number(id)},
        include: [
          {
            model: database.Role,
            as: 'Role'
          }
        ]
      });
      if (user) {
        return res.status(200).json({user});
      } else {
        return res.status(404).send('User with the specified email doesnt exist');
      }
      
    } catch (error) {
      return res.status(500).send(error.message);
    }
  }

  static updateUser(req, res){
    return database.User.findByPk(req.params.id)
    .then(user => {
      if (!user) {
        return res.status(404).send({
          message: 'User not found'
        });
      }
      return user
        .update({
          name: req.body.name || user.name,
          roleId: req.body.roleId || user.roleId,
          email: req.body.email || user.email,
          password: req.body.password || user.password,
          phone_number: req.body.phone_number || user.phone_number,
          service_agreement: req.body.service_agreement || user.service_agreement,
          data_usage_agreement: req.body.data_usage_agreement || user.data_usage_agreement
        })
        .then(() => res.status(200).send({
          status: 'sukses update user',
          message: user
        }))
        .catch((error) => res.status(400).send(error));
    })
    .catch((error) => res.status(400).send(error));
  }

  // static async updateUser(req, res) {
  //   const alteredUser = req.body;
  //   const {id} = req.params;
  //   if (!Number(id)) {
  //       return res.status(400).send('please input valid number');
  //   }
  //   try {
  //       const updateUser = await database.User.findOne({where: {id: Number(id) }
  //     });
  //       if (!updateUser) {
  //           return res.status(404).send('cannot update user')
  //       } else {
  //           // util.setSuccess(200, 'Updated', updateUser);
  //           return res.status(200).json({message: 'Updated', updateUser})
  //       }
  //       // return util.send(res);
  //   } catch (error) {
  //       return status(404).send({message: error.message})
  //   }
  // };

  static async deleteUser(req, res) {
    try {
      const { id } = req.params;
      const deleted = await database.User.destroy({
        where: {id: Number(id) }
      });
      if (deleted) {
        return res.status(204).send('User deleted');
      }
      throw new Error('user not found');
    } catch (error) {
      return res.status(500).send(error.message);
    }
  };

  static async currentUser(req, res) {
    const pass = passport.authenticate("jwt", {session: false});
    if (pass) {
      return res.json({
        email: req.body.email,
      });
    } else {
      return res.send('cannot find user')
    }
    
  }

  static async loginUser(req, res) {
    const login = await database.User.findOne({
      where: {
        email: req.body.email,
        // password: req.body.password
      }
    })
    .then((login) => {
      if (!login) {
        return res.status(401).send({
          message: 'AUTHENTICATION FAILED, user not found'
        });
      }
      login.comparePassword(req.body.password, (error, isMatch) => {
        if(isMatch && !error) {
          var token = jwt.sign(JSON.parse(JSON.stringify(login)), 'nodeauthsecret', {expiresIn: 86400 * 30});
          jwt.verify(token, 'nodeauthsecret', function(error, data){
            console.log(error, data);
          })
          res.json({success: true, token: 'JWT ' + token});
        } else {
          res.status(402).send({succes: false, message: 'Authentication failed, wrong password'});
        }
      })
    })
  }
}

export default UserController;





