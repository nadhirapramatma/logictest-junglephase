import models from '../src/models';
import QuestionService from '../services/questionService';
import Util from '../utils/Utils';

const util = new Util();


class QuestionController {

  //create question
  static async createQuestion(req, res) {
    if (!req.body.question || !req.body.questionCategoryId || !req.body.questionTypeId  ) {
      util.setError(400, 'Please provide complete details');
      return util.send(res);
    }
    const newQuestion = req.body;
    try {
      const createdQuestion = await QuestionService.createQuestion(newQuestion);
      util.setSuccess(201, 'question exam Added!', createdQuestion);
      return util.send(res);
    } catch (error) {
      util.setError(400, error.message);
      return util.send(res);
    }
  }

  //get all question

  static async getAllQuestions  (req, res) {
    try {
      const questions = await models.Question.findAll({
        include: [
          {
            model: models.QuestionType,
            as: 'questionType'
          }
        ]
      });
      return res.status(200).json({ 
        status:'berikut soal yang tersedia',
        message: questions });
    } catch (error) {
      return res.status(500).send(error.message);
    }
  }

  //update question
  static async updateQuestion(req, res) {
    const alteredQuestion = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }
    
    try {
      const updateQuestion = await QuestionService.updateQuestion(id, alteredQuestion);
      if (!updateQuestion) {
        util.setError(404, `Cannot find book with the id: ${id}`);
      } else {
        util.setSuccess(200, 'Book updated', updateQuestion);
      }
      return util.send(res);
    } catch (error) {
      util.setError(404, error);
      return util.send(res);
    }
  }

  //get one question
  static async getQuestionById (req, res)  {
    try {
      const question = await models.Question.findByPk(
        req.params.id,
        {include: [
          {
            model: models.QuestionType,
            as: 'questionType'
          },
          // {
          //   model: models.QuestionCategory,
          //   as: 'questionCategory'
          // }
        ]
      });
      if (question) {
        return res.status(200).json({ question });
      }
      return res.status(404).send('Question with the specified ID does not exists');
    } catch (error) {
      return res.status(500).send(error.message);
    }
  }

  //delete question
  static async deleteQuestion (req, res){
    try {
      const  questionId  = await models.Question.findByPk(req.params.id);
      const deleted = questionId.destroy({
        where: { id: questionId }
      });
      if (deleted) {
        return res.status(204).send({status:"Question deleted"});
      }
      throw new Error("Question not found");
    } catch (error) {
      return res.status(500).send(error.message);
    }
  };

//success update question
  static updateQuestion(req, res) {
    return models.Question.findByPk(req.params.id)      
      .then(question => {
        if (!question) {
          return res.status(404).send({
            message: 'question Not Found',
          });
        }
        return question
          .update({
            questionCategoryId: req.body.questionCategoryId || question.questionCategoryId,
            questionTypeId: req.body.questionTypeId || quesion.questionTypeId,
            question: req.body.question || question.question,
            options: req.body.options || question.options,
            answer: req.body.answer || question.answer,
            difficulty: req.body.difficulty || question.difficulty,
          })
          .then(() => res.status(200).send({
            status:'sukses merubah question',
            message:question}))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  }


}

export default QuestionController;
