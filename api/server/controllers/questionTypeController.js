import models from '../src/models';
import Util from '../utils/Utils';
import questionTypeService from '../services/questionTypeService';

const util = new Util();

class questionTypeController{
  static async addQuestionType(req, res){
    if(!req.body.name){
      util.setError(400, 'name cannot be null');
      return util.send(res);
  }
    const newQuestionType = req.body
    try{
      const createdQuestionType = await questionTypeService.addQuestionType(newQuestionType);
      util.setSuccess(201, 'Question Type success to added!', createdQuestionType);
      return util.send(res);
    }catch(error){
      util.setError(400, error);
      return util.send(res);
    }
  }

  static async getOneQuestionType(req,res){
    const {id} = req.params;
    if (!Number(id)) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res);
    }

    try{
      const theQuestionType = await questionTypeService.getOneQuestionType(id);
      
      if(!theQuestionType){
          util.setError(404, `Cannot find question type with the id ${id}`);
      }else{
          util.setSuccess(200, 'Found Question Type', theQuestionType)
      }
      return util.send(res);
    
    }catch(error){
      util.setError(400, error);
      return util.send(res);
    }

  }
  
  static async updateQuestionTypeById(req, res) {
    const alteredQuestionType = req.body;
    const {id} = req.params;
    if (!Number(id)) {
        util.setError(400, 'Please input a valid numeric value');
        return util.send(res);
    }

    try{
        const updateQuestionType = await questionTypeService.updateQuestionType(id, alteredQuestionType);
        if(!updateQuestionType){
            util.setError(404, `Cannot find question type with the id ${id}`);
        }else{
            util.setSuccess(200, `Question type with the id ${id} was success to updated`, updateQuestionType);
        }
        return util.send(res);
    }catch(error){
        util.setError(400, error);
        return util.send(res);
    }
  }

  static async getAllQuestionTypes(req, res){
    try{
      const allQuestionTypes = await questionTypeService.getAllQuestionTypes();
      if ( allQuestionTypes.length > 0 ){
        util.setSuccess(200, 'These are all question types you need', allQuestionTypes)
      }else{
        util.setSuccess(200, 'No question type found')
      } 
      return util.send(res);
    }catch(error){
      util.setError(400, error)
      return util.send(res);
    }
  }

  static async deleteOneQuestionType (req, res){
    const {id} = req.params;

    if (!Number(id)) {
        util.setError(400, 'Please provide a numeric value');
        return util.send(res);
    }

    try{
        const questionTypeToDelete = await questionTypeService.deleteOneQuestionType(id);

        if (questionTypeToDelete){
            util.setSuccess(200, `Question Type with the id ${id} was deleted`)
        }else{
            util.setError(404, `Question type with the id ${id} cannot be found`)
        }
        return util.send(res);
    }catch(error){
        util.setError(400, error);
        return util.send(res)
    }
        
  }
}

export default questionTypeController;