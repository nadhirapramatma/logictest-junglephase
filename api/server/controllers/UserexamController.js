import models from '../src/models';
import UserExamService from '../services/userExamService';
import Util from '../utils/Utils';

const util = new Util();

class UserexamController {

  static async createUserexam(req, res) {
    if (!req.body.userDetailId || !req.body.testId ) {
      util.setError(400, 'Please provide complete details');
      return util.send(res);
    }
    const newUserexam = req.body;
    try {
      const createdUserexam = await UserExamService.createUserexam(newUserexam);
      util.setSuccess(201, 'User exam Added!', createdUserexam);
      return util.send(res);
    } catch (error) {
      util.setError(400, error.message);
      return util.send(res);
    }
  }


  //get all user exams
  static async getAllUserexams(req, res) {
    try {
      const allUserexams = await UserExamService.getAllUserexams();
      if (allUserexams.length > 0) {
        util.setSuccess(200, 'User exam retrieved', allUserexams);
      } else {
        util.setSuccess(200, 'No User exam found');
      }
      return util.send(res);
    } catch (error) {
      util.setError(400, error);
      return util.send(res);
    }
  }

  // get one userexam
  static async getUserexamById (req, res)  {
    try {
      const userexam = await models.UserExam.findByPk(req.params.id,
        {include: [
          {
            model: models.UserDetail,
            as: 'examine'
          },
          {
            model: models.TestType,
            as: 'test'
          }
        ]
      });
      if (userexam) {
        return res.status(200).json({ userexam});
      }
      return res.status(404).send('user with the specified ID does not exists');
    } catch (error) {
      return res.status(500).send(error.message);
    }
  }

  //Update user exam sukses
  static updateUserExam(req, res) {
    return models.UserExam.findByPk(req.params.id)      
      .then(userexam => {
        if (!userexam) {
          return res.status(404).send({
            message: 'question Not Found',
          });
        }
        return userexam
          .update({
            userDetailId: req.body.userDetailId || userexam.userDetailId,
            testId: req.body.testId || userexam.testId,
            score: req.body.score || userexam.score
          })
          .then(() => res.status(200).send({
            status: 'sukses mengupdate',
            message: userexam}))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  }

  //update userexam score
  static updateUserExamScore(req, res) {
    return models.UserExam.findByPk(req.params.id)      
      .then(userexam => {
        if (!userexam) {
          return res.status(404).send({
            message: 'question Not Found',
          });
        }
        return userexam
          .update({
            userDetailId: userexam.userDetailId,
            testId: userexam.testId,
            score: req.score 
          })
          .then(() => res.status(200).send({
            status: 'sukses mengupdate score',
            message: userexam}))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  }

  //Delete user exam
  static deleteUserexam(req, res) {
    return models.UserExam
      .findByPk(req.params.id)
      .then(userexam => {
        if (!userexam) {
          return res.status(400).send({
            message: 'user exam Not Found',
          });
        }
        return userexam
          .destroy()
          .then(() => res.status(204).send({
            status:'sukses menghapus peserta test',
            message:'sukses'
          }))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  }  

 
  
}

export default UserexamController;

