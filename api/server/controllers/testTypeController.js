import models from '../src/models';
import testTypeService from '../services/testTypeService';
import Util from '../utils/Utils';

const util = new Util();

class testTypeController{

    static async addTestType(req, res){
        if(!req.body.name || !req.body.description || !req.body.instruction || !req.body.statusPublish || !req.body.timer){
            util.setError(400, 'name, description, instruction, status publish and timer cannot be null');
            return util.send(res);
        }
        const newTestType = req.body;
        try{
            const createdTestType = await testTypeService.addTestType(newTestType);
            util.setSuccess(201, 'Test Type success to added!', createdTestType);
            return util.send(res);
        }catch(error){
            util.setError(400, error.message);
            return util.send(res);
        }
    }

    static async updateTestType(req, res) {
        const alteredTestType = req.body;
        const {id} = req.params;
        if (!Number(id)) {
            util.setError(400, 'Please input a valid numeric value');
            return util.send(res);
        }

        try{
            const updateTestType = await testTypeService.updateTestType(id, alteredTestType);
            if(!updateTestType){
                util.setError(404, `Cannot find test type with the id ${id}`);
            }else{
                util.setSuccess(200, `Test type with the id ${id} was success to updated`, updateTestType);
            }
            return util.send(res);
        }catch(error){
            util.setError(400, error);
            return util.send(res);
        }
    }

    static async getOneTestType (req, res){
        const {id} = req.params;
        if (!Number(id)) {
          util.setError(400, 'Please input a valid numeric value');
          return util.send(res);
        }

        try{
            const theTestType = await testTypeService.getOneTestType(id);

            if(!theTestType){
                util.setError(404, `Cannot find test type with the id ${id}`);
            }else{
                util.setSuccess(200, 'Found Test Type', theTestType)
            }
            return util.send(res);
        }catch(error){
            util.setError(400, error);
            return util.send(res);
        }

    }

    static async getAllTestTypes (req, res) {
        try{
            const allTestTypes = await testTypeService.getAllTestType();
            if ( allTestTypes.length > 0 ){
                util.setSuccess(200, 'These are all test types you need', allTestTypes)
            }else{
                util.setSuccess(200, 'No test type found')
            }
            return util.send(res);
        }catch(error){
            util.setError(400, error)
            return util.send(res);
        }
    }

    static async deleteOneTestType (req, res){
        const {id} = req.params;

        if (!Number(id)) {
            util.setError(400, 'Please provide a numeric value');
            return util.send(res);
        }

        try{
            const testTypeToDelete = await testTypeService.deleteTestType(id);

            if (testTypeToDelete){
                util.setSuccess(200, `Test Type with the id ${id} was deleted`)
            }else{
                util.setError(404, `Test type with the id ${id} cannot be found`)
            }
            return util.send(res);
        }catch(error){
            util.setError(400, error);
            return util.send(res)
        }
            
    }
}

export default testTypeController;