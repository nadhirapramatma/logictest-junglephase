import models from '../src/models';


class FormanswerController {

  //create answer with before save
  static async createOneAnswer (req, res) {
    try {
      const formanswer = await models.FormAnswer.build(req.body);
      console.log(formanswer.dataValues, 'from answer')

        const answer = formanswer.answer
        console.log(answer, 'this is answer')

        const qId = formanswer.questionId
        console.log(qId, 'this is question id')
        
        const keyanswer = await models.Question.findOne({where: { id: Number(qId)}})
        console.log(keyanswer, 'this is key answer')
        

      const beforeCreate =  models.FormAnswer.beforeCreate( (formanswer) => {
        if( answer === keyanswer.answer){
          formanswer.isCorrect = true
        }else{
          formanswer.isCorrect = false
        }
        return formanswer.isCorrect
        }
      )
      console.log(beforeCreate, 'this is before create')

      await formanswer.save()
      return res.status(201).json({
        status:'succes to add answer',
        data: formanswer
      });
    } catch (error) {
      return res.status(500).json({error: error.message})
    }
  };


  //trial bulk create  form answer 
  static async createAllAnswersofOneUser (req, res) {
    try {
      const formanswers = await models.FormAnswer.bulkCreate(req.body, 
        models.FormAnswer.beforeBulkCreate((formanswers) => {
          formanswers.forEach((formanswer) => {
            if (formanswer.answer === formanswer.questionId) {
              formanswer.isCorrect = true
            } else {
              formanswer.isCorrect = false;
            }
          });
          }
        ));

      return res.status(201).json({
        formanswers,
      });
    } catch (error) {
      return res.status(500).json({error: error.message})
    }
  };

  //get all answer from all users
  static async getAllAnswer  (req, res) {
    try {
      const answers = await models.FormAnswer.findAll({
        include: [
          {
            model: models.Question,
            as: 'question'
          },
          {
            model: models.UserExam,
            as: 'userexam'
          },
        ]
      });
      console.log(answers, 'answers')
      return res.status(200).json({ answers });
    } catch (error) {
      return res.status(500).send(error.message);
    }
  }


  //get one answer by id
  static async getAnswerById (req, res)  {
    try {
      const answer = await models.FormAnswer.findByPk(
        req.params.id,
        {include: [
          {
            model: models.Question,
            as: 'question'
          },
          {
            model: models.UserExam,
            as: 'userexam'
          },
        ]
      });
      if (answer) {
        return res.status(200).json({ answer });
      }
      return res.status(404).send('answer with the specified ID does not exists');
    } catch (error) {
      return res.status(500).send(error.message);
    }
  }

  
  //delete one answer
  static async deleteAnswer (req, res){
    try {
      const answerId = await models.FormAnswer.findByPk(req.params.id);
      const deleted  = answerId.destroy();
      if (deleted) {
        return res.status(204).send("Answer deleted");
      }
      throw new Error("Answer not found");
    } catch (error) {
      return res.status(500).send(error.message);
    }
  }; 

  //controller for update form answer
  static updateAnswer(req, res) {
    return models.FormAnswer.findByPk(req.params.id)      
      .then(answer => {
        if (!answer) {
          return res.status(404).send({
            message: 'answer Not Found',
          });
        }
        return answer
          .update({
            questionId: req.body.questionId || answer.questionId,
            userId: req.body.userId || answer.userId,
            answer: req.body.answer || answer.answer
          })
          .then(() => res.status(200).send({
            status:'sukses mengupdate jawaban',
            message: answer}))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  }

}
export default FormanswerController;
