import database from '../src/models';
// import Sequelize from 'sequelize';
import Utils from '../utils/Utils';

const util = new Utils();

class userdetailController {
  static async addUserDetail(req, res) {
    try {
      console.log(req.body)
      const newUser = await database.UserDetail.create(req.body);
      return res.status(201).json({
        newUser,
      });
    } catch (error) {
      return res.status(500).json({error: error.message})
    }
  }

  static async getAllUsersDetail(req, res) {
    try {
      const users = await database.UserDetail.findAll({
        include: [
          {
            model: database.User,
            as: 'user'
          }
        ]
      });
      return res.status(200).json({users});
    } catch (error) {
      return res.status(500).send(error.message);
    }
  }

  static async getAUserDetail(req, res) {
    try {
      const {id} = req.params;
      const user = await database.UserDetail.findOne({
        where: { id: Number(id) },
        include: [
          {
            model: database.User,
            as: 'user'
          }
        ]
      });
      if (user) {
        return res.status(200).json({user});
      }
      return res.status(404).send('User detail with the specified id doesnt exist');
    } catch (error) {
      return res.status(500).send(error.message);
    }
  }

  static updateUserDetail(req, res) {
    return database.UserDetail.findByPk(req.params.id)      
      .then(userdetail => {
        if (!userdetail) {
          return res.status(404).send({
            message: 'user detail Not Found',
          });
        }
        return userdetail
          .update({
            fullname: req.body.fullname || userdetail.fullname,
            nickname: req.body.nickname || userdetail.nickname,
            dob: req.body.dob || userdetail.dob,
            last_education: req.body.last_education || userdetail.last_education,
            majors: req.body.majors || userdetail.majors,
            school_name: req.body.school_name || userdetail.school_name,
            city_id: req.body.city_id || userdetail.city_id,
            province_id: req.body.province_id || userdetail.province_id,
            full_address_id: req.body.full_address_id || userdetail.full_address_id,
            domicile_city: req.body.domicile_city || userdetail.domicile_city,
            domicile_province: req.body.domicile_province || userdetail.domicile_province,
            domicile_full_address: req.body.domicile_full_address || userdetail.domicile_full_address,
            join_motivation: req.body.join_motivation || userdetail.join_motivation,
            join_motivation_desc: req.body.join_motivation_desc || userdetail.join_motivation_desc,
            data_usage_agreement: req.body.data_usage_agreement || userdetail.data_usage_agreement
          })
          .then(() => res.status(200).send({
            status: 'update success',
            message: userdetail}))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  }


 
  static async deleteUserDetail(req, res) {
    try {
      console.log(req.body)
      const {id} = req.params;
      const deleted = await database.UserDetail.destroy({
        where: {id: Number(id)}
      });
      if (deleted) {
        return res.status(204).send('User detail deleted');
      }
      throw new Error('user detail not found');
    } catch (error) {
      return res.status(500).send(error.message);
    }
  };
}

export default userdetailController;

